===========================
Django Simple Elasticsearch
===========================

This package provides a simple method of creating Elasticsearch indexes for
Django models. The `original version <https://github.com/jaddison/django-simple-elasticsearch>`_ exists
on GitHub, but this package no longer keeps track of upstream changes. It is maintained solely by P5G.

-----

Documentation
-------------

Original ducomentation: `django-simple-elasticsearch on ReadTheDocs <http://django-simple-elasticsearch.readthedocs.org/>`_.

Features
--------

* class mixin with a set of :code:`@classmethods` used to handle:
 * type mapping definition
 * individual object indexing and deletion
 * bulk object indexing
 * model signal handlers for pre/post_save and pre/post_delete (optional)
* management command to handle index/type mapping initialization and bulk indexing
 * uses Elasticsearch aliases to ease the burden of re-indexing
* small set of Django classes and functions to help deal with Elasticsearch querying
 * base search form class to handle input validation, query preparation and response handling
 * multi-search processor class to batch multiple Elasticsearch queries via :code:`_msearch`
 * 'get' shortcut functions
* post index create/rebuild signals available to perform actions after certain stages (ie. add your own percolators)

Installation
------------

At the command line::

    $ easy_install django-simple-elasticsearch

Or::

    $ pip install django-simple-elasticsearch

License
-------

**django-simple-elasticsearch** is licensed as free software under the BSD license.

Todo
----

* Review search classes - simplify functionality where possible. This may cause breaking changes.
* Tests. Write them.
* Documentation. Write it.
